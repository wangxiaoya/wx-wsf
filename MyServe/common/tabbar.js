//身份一
let tab1 = [{
    "pagePath": "/pages/index/index",
    "iconPath": "/static/tab-ic-hom-unselected.png",
    "selectedIconPath": "/static/tab-ic-hom-selected.png",
    "text": "首页",
  }, {
    "pagePath": "/pages/category/index",
    "iconPath": "/static/tab-ic-classify-selected.png",
    "selectedIconPath": "/static/tab-ic-classify-unselected.png",
    "text": "分类",
  },
  {
    "pagePath": "/pages/order/index",
    "iconPath": "/static/tab-ic-order-unselected.png",
    "selectedIconPath": "/static/tab-ic-order-selected.png",
    "text": "订单",

  }, {
    "pagePath": "/pages/info/index",
    "iconPath": "/static/tab-ic-me-unselected.png",
    "selectedIconPath": "/static/tab-ic-me-selected.png",
    "text": "我的",
  }
]
// 身份二
let tab2 = [{
  "pagePath": "/pages/master/index",
  "iconPath": "/static/tab-ic-order-unselected1.png",
  "selectedIconPath": "/static/tab-ic-order-selected1.png",
  "text": "接单",
}, {
  "pagePath": "/pages/info/index",
  "iconPath": "/static/tab-ic-me-unselected.png",
  "selectedIconPath": "/static/tab-ic-me-selected.png",
  "text": "我的",
}]
export default [
  tab1,
  tab2
]