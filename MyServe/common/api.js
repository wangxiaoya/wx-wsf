import {
  server
} from './request.js'
var api = []
//获取协议和隐私政策
//api.get_policy = (params, apr) => server(`/api-smc/ui-nl/v1/app-aga/${params}`, 'GET', apr)
//code获取手机号
api.getPhoneByCode = (params) => server(`/wx/auth/getPhoneByCode`, 'POST', params)
//获取协议
api.protocolByCode = (params) => server(`/wx/protocol/getByCode`, 'POST', params)
//登录
api.loginByWeixin = (params) => server(`/wx/auth/loginByWeixin`, 'POST', params)
//首页
api.homeInfo = (params) => server(`/wx/home/info`, 'POST', params)
//服务详情
api.service = (params) => server(`/wx/service/${params}`, 'get')
//分类父级目录
api.parent = (params) => server(`/wx/category/tree`, 'POST', params)
//服务列表
api.serviceList = (params) => server(`/wx/service/list`, 'POST', params)
//根据类目ID查询树形列表
api.serviceInfo = (params) => server(`/wx/service/getTreeListByName`, 'POST', params)
//根据类目ID查询树形列表
api.TreeListByCategoryId = (params) => server(`/wx/service/getTreeListByCategoryId`, 'POST', params)
//城市列表
api.region = (params) => server(`/wx/region/tree`, 'POST', params)
//新增购物车
api.addCartService = (params) => server(`/wx/cart/addCartService`, 'POST', params)

//购物车列表
api.cartList = (params) => server(`/wx/cart/userId/${params}`, 'get')
//修改购物车服务
api.editCartService = (params) => server(`/wx/cart/editCartService`, 'put', params)
//删除购物车服务
api.deleteCart = (params) => server(`/wx/cart/removeCartService/${params}`, 'DELETE')
//购物车详情
api.detailsCart = (params) => server(`/wx/cart/${params}`, 'get')
//区域下拉框
api.region = (params) => server(`/wx/region/tree`, 'post', params)

//新增地址
api.address = (params) => server(`/wx/address`, 'post', params)
//地址列表
api.addressList = (params) => server(`/wx/address/list`, 'post', params)
//地址详情接口
api.addressdetaile = (params) => server(`/wx/address/${params}`, 'get')
//地址修改接口
api.addressEdit = (params) => server(`/wx/address`, 'put', params)
//删除地址
api.addressDelete = (params) => server(`/wx/address/${params}`, 'DELETE')
//直接下单
api.orderService = (params) => server(`/wx/service/${params}`, 'get')
//新增订单接口
api.order = (params) => server(`/wx/order`, 'post', params)
//订单查询
api.ordersDetails = (params) => server(`/wx/order/${params}`, 'get')
//订单列表接口
api.orderUserId = (params) => server(`/wx/order/userId/${params}`, 'get')
//订单支付
api.orderPay = (params) => server(`/wx/order/pay`, 'post', params)
//预支付
api.orderIdPay = (params) => server(`/wx/order/prePay/${params}`, 'get')
//已支付订单
api.payList = (params) => server(`/wx/order/payList`, 'post', params)
//接单页面
api.addorderMaster = (params) => server(`/wx/master/receiveOrder`, 'post', params)
//接单列表
api.orderList = (params) => server(`/wx/master/orderList/${params}`, 'get')
//修改师傅接口
api.editOrderMaster = (params) => server(`/wx/master/editOrderMaster`, 'post', params)
//清除token
api.removeToken = (params) => server(`/wx/auth/removeToken`, 'post', params)
//新增意见反馈
api.feedback = (params) => server(`/wx/feedback`, 'post', params)
//反馈列表
api.feedbackList = (params) => server(`/wx/feedback/list`, 'post', params)
//字典（类型）
api.dictList = (params) => server(`/system/dict/data/type/${params}`, 'get')
//根据订单ID查询师傅
api.orderMaster = (params) => server(`/wx/order/orderMaster/${params}`, 'get')
//新增用户评论
api.comment = (params) => server(`/wx/comment`, 'post', params)
//查询评论
api.commentList = (params) => server(`/wx/comment/list`, 'post', params)

export default {
  api
}