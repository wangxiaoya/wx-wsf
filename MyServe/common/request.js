var baseUrl = 'https://api.zzzhuoya.com:443'
//var baseUrl = 'http://192.168.2.100:6126'
export function server(url, method, params, tkn = false) {
  uni.showLoading({
    title: '加载中'
  })
  let promise = new Promise((resolve, reject) => {
    // 这里是获取 token 
    let token = uni.getStorageSync("userInfo")
    let access_token = uni.getStorageSync("token")
    uni.request({
      url: baseUrl + url,
      method: method,
      data: params,
      dataType: "json",
      timeout: 60000,
      header: {
        'content-type': method == 'POST' ? 'application/json;charset=UTF-8' : 'application/json',
        'Authorization': token.token ? `Bearer ${token.token}` : '',
      },

      // 这里请求成功根据自己的 code 判断就好了
      success: function(res) { // 当请求成功时调用的函数。这个函数会得到一个参数：从服务器返回的数据。当请求成功时调用函数，即status==200。
        // if(res.data.code==401){
        // 	// console.log(res)
        // 	uni.removeStorageSync('token')
        // }
        resolve(res.data)
        uni.hideLoading();
      },
      fail: function(res) {
        reject(res)
        setTimeout(() => {
          uni.hideLoading()
        }, 8000)
      },
      complete: function(res) {}
    });
  });
  return promise;
}