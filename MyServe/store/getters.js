const getters = {
  navTabList: (state) => state.navTabList,
  userInfo: (state) => state.userInfo,
  activeMenuPath: (state) => state.activeMenuPath,
  isCollapse: (state) => state.isCollapse,
  sysMsgCountList: (state) => state.sysMsgCountList,
  globalConfig: (state) => state.globalConfig,
  menuList: (state) => state.menuList,
  pageSizes: (state) => state.globalConfig.pageSizes,
  pageSize: (state) => state.globalConfig.pageSize,
  favico: (state) => state.favico,

}
export default getters