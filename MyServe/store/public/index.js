export default {
  state: {
    tabBarList: []
  },
  actions: {
    changeTabbar({
      commit
    }, payload) {
      // 使用commit方法提交一个mutation，更新state中的tabBarList
      commit('updateTabbar', payload)
    }
  },
  mutations: {
    updateTabbar(state, payload) {
      state.tabBarList = payload
    }
  }
  getters: {
    tabBarList: (state) => state.tabBarList,
  },
}