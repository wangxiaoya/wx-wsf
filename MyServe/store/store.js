import Vue from 'vue'
import Vuex from 'vuex'
import tabbarRole from '@/store/tabbarRole.js'
import getters from '@/store/getters.js'
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    tabbarRole
  },
  getters
})

export default store