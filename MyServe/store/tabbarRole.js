import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    tabBarList: uni.getStorageSync('tabBarList', ) ? uni.getStorageSync('tabBarList') : []
  },
  mutations: {
    add(state, payload) {
      state.tabBarList = payload
      uni.setStorageSync('tabBarList', payload)
    }
  },
  actions: {
    changeTabbar(state, payload) {
      state.tabBarList = payload
      state.commit('add', payload)
      uni.setStorageSync('tabBarList', payload)
      // console.log(state.tabBarList, '&&&&')
    }
  },
  getters: {
    tabBarList: state => state.tabBarList,
  }
})
export default store