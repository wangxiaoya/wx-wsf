import App from './App'

// #ifndef VUE3
import Vue from 'vue'
import './uni.promisify.adaptor'
import serve from './common/api.js'
import store from '@/store/tabbarRole.js'
import {
  hiddenPhone,
  preview,
  deleteHtmlTag,
  baseUrl
} from './common/util.js'
// 全局调用请求
Vue.prototype.$http = serve
Vue.prototype.$hiddenPhone = hiddenPhone
Vue.prototype.$preview = preview
Vue.prototype.$gaodeKey = 'TG2BZ-L6GWG-FCBQX-QBXJQ-AIA53-J6B2P'; //高德地图key
Vue.prototype.$deleteHtmlTag = deleteHtmlTag
Vue.prototype.baseUrl = baseUrl
Vue.prototype.$store = store

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
  store,
  ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import {
  createSSRApp
} from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif